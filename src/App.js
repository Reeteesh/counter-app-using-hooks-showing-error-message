import React from 'react'

const App = () => {
  const [count, setCount] = React.useState(0);
  const [message, setMessage] = React.useState("");


  const handleAdd = () => {
    setCount(count + 1);
    setMessage("Adding Process WIP");

  }
  const handleSub = () => {
    if (count > 0) {
      setCount(count - 1);
      setMessage("Negative Process WIP");

    } else {
      setMessage("Value cannot be less than 0");
    }
  }
  return (
    <div>
      {count}
      <button onClick={() => { handleAdd() }}>Add</button>
      <button onClick={() => { handleSub() }}>Minus</button>
      {message}
    </div>
  )
}

export default App
